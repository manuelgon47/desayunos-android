package com.mgonzalez.mg.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.activities.NoConnectionActivity;
import com.mgonzalez.desayunos.activities.SplashScreenActivity;
import com.mgonzalez.desayunos.fragment.ParentFragment;
import com.mgonzalez.desayunos.utils.Constants;

public class MGParentActivity extends FragmentActivity {

	private ProgressDialog pd;
	private int espera = 0;
	public AQuery aq;
	public String currentFragmentTag;

	@Override
	protected void onResume() {
		super.onResume();
		if (!(this instanceof NoConnectionActivity) && !(this instanceof SplashScreenActivity) && !isNetworkAvailable()) {
			openNoConnection();
		}

	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		if (Constants.DEBUG_MODE) {
			Log.d(Constants.TAG_DEBUG, "Clase: " + this + "Estado de la red: " + (activeNetworkInfo != null && activeNetworkInfo.isConnected()));
		}
		if (activeNetworkInfo == null) {
			return false;
		} else {
			return activeNetworkInfo.isConnectedOrConnecting();
		}
	}

	private void openNoConnection() {
		Intent intent = new Intent(this, NoConnectionActivity.class);
		startActivity(intent);
	}

	public ParentFragment getCurrentFragment(int fragmentWrapperResId) {
		ParentFragment res = null;

		FragmentManager fragmentManager = getSupportFragmentManager();
		res = (ParentFragment) fragmentManager.findFragmentById(fragmentWrapperResId);

		if (res != null && res.isHidden()) {
			if (currentFragmentTag != null) {
				res = (ParentFragment) fragmentManager.findFragmentByTag(currentFragmentTag);
			}
		}

		return res;
	}
	
	//*******************
	//*		Loading		*
	//*******************
	public void showLoading() {
		showLoading(R.string.loading);
	}
	
	public void showLoading(int resMsg) {
		showLoading(getString(resMsg));
	}

	public void showLoading(String msg) {
		pd = ProgressDialog.show(this, "", msg, true, false);
		espera++;
	}

	public void hideLoading() {
		if (pd.isShowing() && espera > 0) {
			pd.dismiss();
		}
	}
	
	// **********************
	// *		Dialogs		*
	// **********************
	public interface MGDialogListener {
		public void acceptClick(DialogInterface dialog);
		public void cancelClick(DialogInterface dialog);
	}
	
//	public void showDialog(String message) {
//		showDialog(message, null);
//	}
	
	public void showDialog(int resMessage, final MGDialogListener listener) {
		showDialog(resMessage, R.string.accept, R.string.cancel, listener);
	}
	
//	public void showDialog(String message, final MGDialogListener listener) {
//		showDialog(message, listener);
//	}
	
	public void showDialog(int message, int acceptButton, int cancelButton, final MGDialogListener listener) {
		showDialog(getString(R.string.warning), getString(message), getString(acceptButton), getString(cancelButton), listener);
	}
	
	public void showDialog(String message, String acceptButton, String cancelButton, final MGDialogListener listener) {
		showDialog(message, acceptButton, cancelButton, listener);
	}
	
	public void showDialog(String title, String message, String acceptButton, String cancelButton, final MGDialogListener listener) {
		new AlertDialog.Builder(this)
//        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(acceptButton, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if(listener != null) {
            		listener.acceptClick(dialog);
            	}
            }

        })
        .setNegativeButton(cancelButton, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
            	if(listener != null) {
            		listener.cancelClick(dialog);
            	}
            }

        })
        .show();
	}

	// *******************
	// * 	FRAGMENTS	 *
	// *******************
	public void openFragment(ParentFragment fragment) {
		openFragment(fragment, Constants.FRAGMENT_TAG_WRAPPER_ID, R.anim.slide_left_in, R.anim.slide_left_out, R.anim.slide_right_in, R.anim.slide_right_out, true);
	}

	public void openFragment(ParentFragment fragment, boolean addToBackStack) {
		openFragment(fragment, Constants.FRAGMENT_TAG_WRAPPER_ID, 0, 0, 0, 0, addToBackStack);
	}

	public void openFragment(ParentFragment fragment, int enterAnim, int exitAnim, boolean addToBackStack) {
		openFragment(fragment, Constants.FRAGMENT_TAG_WRAPPER_ID, enterAnim, exitAnim, enterAnim, exitAnim, addToBackStack);
	}

	public void openFragment(ParentFragment fragment, int fragmentWrapperResId) {
		openFragment(fragment, fragmentWrapperResId, R.anim.slide_left_in, R.anim.slide_left_out, R.anim.slide_right_in, R.anim.slide_right_out, true);
	}

	public void openFragment(ParentFragment fragment, int fragmentWrapperResId, int enterAnim, int exitAnim, int popEnterAnim, int popExitAnim, boolean addToBackStack) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		ParentFragment currentFragment = getCurrentFragment(fragmentWrapperResId);
		if (currentFragment != null && currentFragment.getTagName().equals(fragment.getTagName())) {
			return;
		}

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim);

		if (currentFragment != null) {
			transaction.hide(currentFragment);
		}

		if (fragment.isAdded()) {
			transaction.show(fragment);
		} else {
			transaction.add(fragmentWrapperResId, fragment, fragment.getTagName()).setBreadCrumbShortTitle(fragment.getTagName());
		}

		if (addToBackStack) {
			transaction.addToBackStack(fragment.getTagName());
		} else {
			currentFragmentTag = fragment.getTagName();
		}

		transaction.commit();
	}

}
