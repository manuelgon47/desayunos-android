package com.mgonzalez.mg.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.activities.ParentActivity;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.fragment.ParentFragment;

public class MGParentFragment extends Fragment {
	
	public AQuery aq;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, int layoutResId) {
		View view = inflater.inflate(layoutResId, container, false);
		
		aq = new AQuery(getActivity(), view);
		
		return view;
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden) {
			onShowView();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(!isHidden()) {
			onShowView();
		}
	}
	
	public void onShowView() {
		// TODO:
	}
	
	public String getTagName() {
		return getClass().getSimpleName() + System.identityHashCode(this);
	}
	
	public void showToast(int resMsg) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showToast(resMsg);
		}
	}
	
	public void showToast(String msg) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showToast(msg);
		}
	}
	
	public void showLoading() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showLoading();
		}
	}
	
	public void showLoading(int msg) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showLoading(msg);
		}
	}
	
	public void showLoading(String msg) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showLoading(msg);
		}
	}
	
	public void hideLoading() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.hideLoading();
		}
	}
	
	public void navigateBack() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.onBackPressed();
		}
	}
	
	// *******************
	// * 	FRAGMENTS	 *
	// *******************
	public void openFragment(ParentFragment fragment) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.openFragment(fragment);
		}
	}

	public void openFragment(ParentFragment fragment, boolean addToBackStack) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.openFragment(fragment, addToBackStack);
		}
	}
	
	//*******************
	//*		Intents		*
	//*******************

}
