package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.partials.NavigationBar;

public class GoBreakfastFragment extends ParentFragment {
	
	public static boolean reload = false;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.go_breakfast);
		
		UsuarioAutenticadoDto user = MGApplication.getInstance().getUsuarioAutenticado();
		if(user != null) {
			if(user.getActualPartnerId() == 0) {
				aq.id(R.id.btnGoWithParner).gone();
			} else {
				aq.id(R.id.btnGoWithParner).visible().text(getString(R.string.go_with_partner, user.getActualPartnerName()));
			}
		}
		
		aq.id(R.id.btnGoAlone).clicked(this, "goAlone");
		aq.id(R.id.btnGoWithParner).clicked(this, "goWithMyPartner");
		aq.id(R.id.btnGoWithOther).clicked(this, "goWithOther");
		
		return view;
	}
	
	public void goAlone() {
		goBreakfast(new HashMap<String, Object>());
		UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
		usuarioAutenticado.setActualPartnerId(0);
		usuarioAutenticado.setActualPartnerName("");
		MGApplication.getInstance().setUsuarioAutenticado(usuarioAutenticado);
		aq.id(R.id.btnGoWithParner).gone();
	}
	
	public void goWithMyPartner() {
		Map<String, Object> params = new HashMap<String, Object>();
		UsuarioAutenticadoDto user = MGApplication.getInstance().getUsuarioAutenticado();
		if(user != null) {
			params.put("idCompa", user.getActualPartnerId());
		}
		goBreakfast(params);
	}
	
	public void goWithOther() {
		//openIntent(new Intent(this, ChangePartnerActivity.class));
		ParentFragment fragment = new ChangePartnerFragment();
		openFragment(fragment);
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.go_breakfast);
	}


}
