package com.mgonzalez.desayunos.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.partials.NavigationBar;

public class ChangeBreakfastGroupFragment extends ParentFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout._change_breakfast_group);
		
		aq.id(R.id.btnCreateGroup).clicked(this, "createGroup");
		aq.id(R.id.btnSearchGroup).clicked(this, "searchGroup");
		
		return view;
	}
	
	public void createGroup() {
		ParentFragment fragment = new CreateBreakfastGroupFragment();
		openFragment(fragment);
	}
	
	public void searchGroup() {
		ParentFragment fragment = new SearchBreakfastGroupFragment();
		openFragment(fragment);
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.change_breakfast_group);
		navBar.setNavigationBarButtonLeft(R.drawable.icon_menu);
	}

}
