package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.dto.GrupoDto;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.utils.Constants;

public class SearchBreakfastGroupFragment extends ParentFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.search_breakfast_group);
		
		aq.id(R.id.btnSearchGroup).clicked(this, "search");
		
		return view;
	}
	
	public void search() {
		String nombreGrupo = aq.id(R.id.editSearchName).getText().toString();
		String emailAdmin = aq.id(R.id.editSearchMail).getText().toString();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nombreGrupo", nombreGrupo);
		params.put("emailAdmin", emailAdmin);
		
		showLoading();
		ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_SEARCH_BREAKFAST_GROUP, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					List<GrupoDto> grupos = GrupoDto.loadGrupos(result);
					SearchResultsBreakfastGroupFragment fragment = new SearchResultsBreakfastGroupFragment();
					fragment.loadGrupos(grupos);
					openFragment(fragment);
				}
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.search_breakfast_group);
	}

}
