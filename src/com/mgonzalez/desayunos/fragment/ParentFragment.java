package com.mgonzalez.desayunos.fragment;

import java.util.Map;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mgonzalez.desayunos.activities.ParentActivity;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.utils.Constants;
import com.mgonzalez.mg.fragment.MGParentFragment;

public abstract class ParentFragment extends MGParentFragment {
	
	public abstract void setNavigationBar(NavigationBar navBar);
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState, int layoutResId) {
		View view = super.onCreateView(inflater, container, savedInstanceState, layoutResId);
		
		Tracker t = GoogleAnalytics.getInstance(getActivity()).newTracker(Constants.ANALYTICS_ID);
		if(t != null) {
			// Set screen name where path is a String representing the screen name.
			t.setScreenName(this.toString());
			// Send a screen view.
			t.send(new HitBuilders.AppViewBuilder().build());
		} else {
			Log.e(Constants.TAG_DEBUG, "Tracker no inicializado");
		}

		return view;
	}
	
	@Override
	public void onShowView() {
		super.onShowView();
		
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			NavigationBar navBar = activity.getNavBar();
			if(navBar != null) {
				setNavigationBar(navBar);
			}
		}
	}
	
	public void goBreakfast(Map<String, Object> params) {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.goBreakfast(params);
		}
	}
}
