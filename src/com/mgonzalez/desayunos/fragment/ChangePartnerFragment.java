package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.dto.UsuarioDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.partials.RowUser;
import com.mgonzalez.desayunos.utils.Constants;

public class ChangePartnerFragment extends ParentFragment {
	
	private int currentUserSelected;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.change_partner);
		aq.id(R.id.btnChangePartner).clicked(this, "changePartner");
		
		loadGroupUsers();

		return view;
	}
	
	private void displayGrupo(JSONArray grupo) {
		RadioGroup wrapper = (RadioGroup) aq.id(R.id.groupCompas).getView();
		if(grupo == null || wrapper == null) {
			Log.e(Constants.TAG_DEBUG, "Error: No se ha recuperado correctamente al grupo de usuarios o el RadioGroup es nulo");
			return;
		}
		for(int i = 0; i < grupo.length(); i++) {
			final UsuarioDto usuario = UsuarioDto.loadUser(grupo.optJSONObject(i));
			if(usuario != null) {
				if(currentUserSelected == 0) {
					currentUserSelected = usuario.getUsuarioId();
				}
				RowUser row = (RowUser) View.inflate(getActivity(), R.layout._row_user, null);
				if(row != null) {
					row.setRow(usuario);
					row.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							currentUserSelected = usuario.getUsuarioId();
						}
					});
					wrapper.addView(row);
				}
			}
		}
	}
	
	private void loadGroupUsers(){
		showLoading();
		ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_GET_GROUP_USERS, new HashMap<String, Object>(), new ParentDtoListener() {

			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONArray grupo = result.optJSONArray("grupo");
					displayGrupo(grupo);
				}			
			}

			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				hideLoading();
			}
			
		});
	}
	
	public void changePartner() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idCompa", currentUserSelected);
		
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_COMPARTE, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				GoBreakfastFragment.reload = true;
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject user = result.optJSONObject("usuario");
					UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
					usuarioAutenticado.setActualPartnerId(user.optInt("actualPartnerId", -1));
					usuarioAutenticado.setActualPartnerName(user.optString("actualPartnerName", ""));
					MGApplication.getInstance().setUsuarioAutenticado(usuarioAutenticado);
				}
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.change_partner);
	}


}
