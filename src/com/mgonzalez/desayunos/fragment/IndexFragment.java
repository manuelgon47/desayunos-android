package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;

import org.json.JSONObject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.activities.ParentActivity;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.utils.Constants;
import com.mgonzalez.mg.activity.MGParentActivity.MGDialogListener;

public class IndexFragment extends ParentFragment {

	public static final String PARAM_RELOAD = "reloadData";
	
	public static boolean reload = false;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.index);

		//setData();
		loadUserData();
		
		aq.id(R.id.btnAvisarCompas).clicked(this, "sendPushCompas");
		
		return view;
	}
	private void setData() {
		UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
		if(usuarioAutenticado != null) {
			String grupo = usuarioAutenticado.getGrupo();
			if(grupo == null || "".equals(grupo)) {
				aq.id(R.id.textMyGroup).text(R.string.no_grupo);
			} else {
				aq.id(R.id.textMyGroup).text(R.string.mi_grupo, grupo);
			}
			aq.id(R.id.textDesayunoComida).text(R.string.mi_desayuno_comida, usuarioAutenticado.getDesayunoComida());
			aq.id(R.id.textDesayunoBebida).text(R.string.mi_desayuno_bebida, usuarioAutenticado.getDesayunoBebida());
			boolean voy = usuarioAutenticado.isVoyADesayunar();
			int iconVoy = voy?R.drawable.icon_tick:R.drawable.icon_cross;
			aq.id(R.id.textVoyADesayunar).text(voy?R.string.voy:R.string.no_voy).textColor(voy?getResources().getColor(R.color.green):getResources().getColor(R.color.red));
			TextView textVoy = aq.id(R.id.textVoyADesayunar).getTextView();
			if(textVoy != null) {
				textVoy.setCompoundDrawablesWithIntrinsicBounds(iconVoy, 0, 0, 0);
			}
			
			boolean meToca = usuarioAutenticado.isMeTocaPagar();
			if(usuarioAutenticado.getActualPartnerId() == 0) {
				aq.id(R.id.textMeToca).gone();
				aq.id(R.id.btnCambiarUltimoEnPagar).gone();
			} else{
				if(meToca) {
					aq.id(R.id.textMeToca).visible().text(R.string.me_toca_pagar);
				} else {
					aq.id(R.id.textMeToca).visible().text(R.string.le_toca_a, usuarioAutenticado.getActualPartnerName());
				}
				aq.id(R.id.btnCambiarUltimoEnPagar).visible().clicked(this, "cambiarUltimoEnPagar");
			}
		}
	}
	
	private void loadUserData() {
		showLoading();
		ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_USER, new HashMap<String, Object>(), new ParentDtoListener() {

			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject usuario = result.optJSONObject("usuario");
					if(usuario != null) {
						MGApplication.getInstance().setUsuarioAutenticado(usuario);
						setData();
					} else {
						showToast(R.string.error_inesperado);
					}
				}			
			}

			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				hideLoading();
			}
			
		});
	}
	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.index);
	}
	
	public void sendPushCompas() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showDialog(R.string.avisar_compas_confirm, new MGDialogListener() {
				
				@Override
				public void cancelClick(DialogInterface dialog) {
					dialog.dismiss();
				}
				
				@Override
				public void acceptClick(DialogInterface dialog) {
					dialog.dismiss();
					ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_PUSH_MY_GROUP, new HashMap<String, Object>(), null);
				}
			});
		}
	}
	
	public void cambiarUltimoEnPagar() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showDialog(R.string.cambiar_ultimo_pagar_confirm, new MGDialogListener() {
				
				@Override
				public void cancelClick(DialogInterface dialog) {
					dialog.dismiss();
				}
				
				@Override
				public void acceptClick(DialogInterface dialog) {
					dialog.dismiss();
					showLoading();
					ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_CHANGE_PARTNER_ME_TOCA, new HashMap<String, Object>(), new ParentDtoListener() {
						
						@Override
						public void onPostExecuteRemoteCall(JSONObject result) {
							hideLoading();
							loadUserData();
						}
						
						@Override
						public void onFailureExecuteRemoteCall(String msg, int httpCode) {
							hideLoading();
						}
					});
				}
			});
		}
	}
	
}
