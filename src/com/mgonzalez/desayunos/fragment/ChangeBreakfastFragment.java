package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.activities.IndexActivity;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.utils.Constants;

public class ChangeBreakfastFragment extends ParentFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.change_breakfast);
		
		setData();
		
		aq.id(R.id.btnSave).clicked(this, "changeBreakfsat");

		return view;
	}
	
	private void setData() {
		UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
		if(usuarioAutenticado != null) {
			aq.id(R.id.textMyGroup).text(usuarioAutenticado.getGrupo());
			aq.id(R.id.editDesayunoComida).text(usuarioAutenticado.getDesayunoComida());
			aq.id(R.id.editDesayunoBebida).text(usuarioAutenticado.getDesayunoBebida());
		}
	}
	
	public void changeBreakfsat() {
		String tostada = aq.id(R.id.editDesayunoComida).getText().toString();
		String bebida = aq.id(R.id.editDesayunoBebida).getText().toString();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("desayunoComida", tostada);
		params.put("desayunoBebida", bebida);
		
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_UPDATE_BREAKFAST, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				IndexActivity.reload = true;
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject usuario = result.optJSONObject("usuario");
					if(usuario != null) {
						// TODO: Update usuarioAutenticado
						String tostada = usuario.optString("desayunoComida", "");
						String bebida = usuario.optString("desayunoBebida", "");
						
						UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
						usuarioAutenticado.setDesayunoBebida(bebida);
						usuarioAutenticado.setDesayunoComida(tostada);
						MGApplication.getInstance().setUsuarioAutenticado(usuarioAutenticado);
						
						//navigateBack();
					} else {
						showToast(R.string.error_inesperado);
					}
				}
				
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String message, int httpCode) {
				hideLoading();
			}
		});
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.change_breakfast);
		navBar.setNavigationBarButtonLeft(R.drawable.icon_menu);
	}

}
