package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.GrupoDto;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.partials.RowGroup;
import com.mgonzalez.desayunos.utils.Constants;

public class SearchResultsBreakfastGroupFragment extends ParentFragment {

	private int groupIdSelected;
	private List<GrupoDto> grupos;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.search_results_breakfast_group);
		
		groupIdSelected = 0;
		
		loadData();
		
		aq.id(R.id.btnSave).clicked(this, "save");
		
		return view;
	}
	
	public void loadGrupos(List<GrupoDto> grupos) {
		this.grupos = grupos;
	}
	
	public void loadData() {
		if(this.grupos == null) {
			Log.e(Constants.TAG_DEBUG, "Datos no cargados");
			return;
		}
		RadioGroup wrapperGrupos = (RadioGroup)aq.id(R.id.wrapperGrupos).getView();
		if(wrapperGrupos != null) {
			wrapperGrupos.removeAllViews();
			for(final GrupoDto grupo : grupos) {
				RowGroup row = (RowGroup) View.inflate(getActivity(), R.layout._row_group, null);
				if(row != null) {
					row.setRow(grupo);
					row.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							groupIdSelected = grupo.getGrupoId();
						}
					});
					wrapperGrupos.addView(row);
				}
			}
			//Marcamos como checked el row que corresponda a nuestro grupo de desayuno
			checkActualGroup();
		}
	}
	
	private void checkActualGroup() {
		UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
		RadioGroup wrapperGrupos = (RadioGroup)aq.id(R.id.wrapperGrupos).getView();
		if(usuarioAutenticado != null && wrapperGrupos != null) {
			for(int i = 0; i < wrapperGrupos.getChildCount(); i++) {
				View view = wrapperGrupos.getChildAt(i);
				if(view != null && view instanceof RowGroup) {
					RowGroup row = (RowGroup)view;
					GrupoDto grupo = row.getGrupoDto();
					if(grupo != null && usuarioAutenticado.getGrupoId() == grupo.getGrupoId()) {
						row.performClick();
					}
				}
			}
//			row.setChecked(usuarioAutenticado.getGrupoId() == grupo.getGrupoId());
		}

	}
	
	/*private void loadData() {
		showLoading();
		ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_GET_ALL_GROUPS, new HashMap<String, Object>(), new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					List<GrupoDto> grupos = GrupoDto.loadGrupos(result);
					loadGrupos(grupos);
				}
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				// TODO Auto-generated method stub
				
			}
		});
	}*/
	
	public void save() {
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grupoId", groupIdSelected);
		
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_UPDATE_GROUP, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject usuario = result.optJSONObject("usuario");
					if(usuario != null) {
						// TODO: Update usuarioAutenticado
						Integer grupoId = usuario.optInt("grupoId", 0);
						String grupo = usuario.optString("grupo", "");
						
						UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
						usuarioAutenticado.setGrupoId(grupoId);
						usuarioAutenticado.setGrupo(grupo);
						MGApplication.getInstance().setUsuarioAutenticado(usuarioAutenticado);
						
						navigateBack();
					} else {
						showToast(R.string.error_inesperado);
					}
				}
				
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String message, int httpCode) {
				hideLoading();
			}
		});
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.search_breakfast_group);
	}

}
