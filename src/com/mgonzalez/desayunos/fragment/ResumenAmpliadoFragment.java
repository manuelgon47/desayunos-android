package com.mgonzalez.desayunos.fragment;

import org.json.JSONArray;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.dto.RowResumenDto;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.partials.RowResumenAmpliado;
import com.mgonzalez.desayunos.utils.Constants;

public class ResumenAmpliadoFragment extends ParentFragment {
	
	private JSONArray resumen;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.resumen_apliado);
		
		showData();
		
		return view;
	}
	
	public void setResumen(JSONArray resumen) {
		this.resumen = resumen;
	}
	
	private void showData() {
		if(resumen == null) {
			Log.e(Constants.TAG_DEBUG, "EL array de resumen es null");
			return;
		}
		LinearLayout wrapperResumen = (LinearLayout)aq.id(R.id.wrapperResumenAmpliado).getView();
		if(wrapperResumen != null) {
			wrapperResumen.removeAllViews();
			// Pedidos individuales
			for(int i = 0; i < resumen.length(); i++) {
				//RowPedidoIndividual row = (RowPedidoIndividual) View.inflate(getActivity(), R.layout._row_pedido_individual, null);
				RowResumenAmpliado row = (RowResumenAmpliado) View.inflate(getActivity(), R.layout.row_resumen_ampliado, null);
				if(row != null) {
					RowResumenDto rowData = RowResumenDto.parseRowResumenDto(resumen.optJSONObject(i));
					row.setRow(rowData);
				}
				wrapperResumen.addView(row);
			}
			
		}
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.resumen);
	}

}
