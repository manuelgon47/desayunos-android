package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.partials.RowBebida;
import com.mgonzalez.desayunos.partials.RowPedidoIndividual;
import com.mgonzalez.desayunos.partials.RowTostada;
import com.mgonzalez.desayunos.utils.Constants;

public class ResumenFragment extends ParentFragment {
	
	private JSONArray resumen;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout.resumen);

		loadResumen();
		
		aq.id(R.id.btnResumenAmpliado).clicked(this, "openResumenAmpliado");

		return view;
	}
	
	public void openResumenAmpliado() {
		ResumenAmpliadoFragment fragment = new ResumenAmpliadoFragment();
		fragment.setResumen(resumen);
		openFragment(fragment);
	}
	
	private void loadGrupos(JSONArray resumen) {
		if(resumen == null) {
			Log.e(Constants.TAG_DEBUG, "Eror: El JSONArray de resumen viene null");
			return;
		}

		LinearLayout wrapperBebidas = (LinearLayout)aq.id(R.id.wrapperResumenBebidas).getView();
		LinearLayout wrapperTostadas = (LinearLayout)aq.id(R.id.wrapperResumenTostadas).getView();
		LinearLayout wrapperPedidios = (LinearLayout)aq.id(R.id.wrapperResumenPedidosIndividuales).getView();
		if(wrapperBebidas == null || wrapperTostadas == null || wrapperPedidios == null) {
			Log.e(Constants.TAG_DEBUG, "Eror: No se han podido recuperar los wrapper´s de bebidas y tostadas");
			return;
		}
		
		if(resumen.length() == 3) {
			JSONArray bebidas = resumen.optJSONArray(0);
			JSONArray tostadas = resumen.optJSONArray(1);
			JSONArray pedidiosIndividuales = resumen.optJSONArray(2);
			this.resumen = pedidiosIndividuales;
			// Bebidas
			for(int i = 0; i < bebidas.length(); i++) {
				RowBebida row = (RowBebida) View.inflate(getActivity(), R.layout._row_bebida, null);
				if((row != null)) {
					row.setRow(bebidas.optString(i, ""));
					wrapperBebidas.addView(row);
				}
			}
			
			// Tostadas
			for(int i = 0; i < tostadas.length(); i++) {
				RowTostada row = (RowTostada) View.inflate(getActivity(), R.layout._row_tostada, null);
				if((row != null)) {
					row.setRow(tostadas.optString(i, ""));
					wrapperTostadas.addView(row);
				}
			}
			
			// Pedidos individuales
			for(int i = 0; i < pedidiosIndividuales.length(); i++) {
				RowPedidoIndividual row = (RowPedidoIndividual) View.inflate(getActivity(), R.layout._row_pedido_individual, null);
				if((row != null)) {
					row.setRow(pedidiosIndividuales.optJSONObject(i));
					wrapperPedidios.addView(row);
				}
			}
			
		} else {
			Log.e(Constants.TAG_DEBUG, "Eror: El JSONArray de resumen viene tiene más de 2 objetos o menos de 2. Deberia de de venir con 2 uno con las bebida y otro con las tostadas");
			return;
		}
	}
	
	private void loadResumen() {
		showLoading();
		ParentDto.getInstance().remoteCallGet(Constants.URL, Constants.METHOD_RESUMEN, new HashMap<String, Object>(), new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONArray resumen = result.optJSONArray("resumen");
					loadGrupos(resumen);
				}
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.resumen);
	}
	
}
