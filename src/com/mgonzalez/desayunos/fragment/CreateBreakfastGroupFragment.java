package com.mgonzalez.desayunos.fragment;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.utils.Constants;

public class CreateBreakfastGroupFragment extends ParentFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState, R.layout._create_breakfast_group);
		
		aq.id(R.id.btnSave).clicked(this, "createGroup");
		
		return view;
	}
	
	public void createGroup() {
		String nombreGrupo = aq.id(R.id.editGroupName).getText().toString();
		if("".equals(nombreGrupo)) {
			showToast("Especifique un nombre para el grupo");
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nombreGrupo", nombreGrupo);
		
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_CREATRE_GROUP, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				if("correcto".equals(estado)) {
					showToast(result.optString("msg", ""));
					navigateBack();
				}
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				showToast("Se ha producido un error, por favor inténtelo de nuevo más tarde");
				hideLoading();
			}
		});
		
	}

	@Override
	public void setNavigationBar(NavigationBar navBar) {
		navBar.setNavigationBarTitle(R.string.create_breakfast_group);
	}

}
