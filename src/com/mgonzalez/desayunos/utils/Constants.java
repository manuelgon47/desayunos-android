package com.mgonzalez.desayunos.utils;

import com.mgonzalez.desayunos.R;



public class Constants {
	public static final boolean DEBUG_MODE = false;
	
	// URL´s
//	public static final String URL = "http://192.168.1.12/desayunos/api_v1/usuario.php?mg=";
//	public static final String URL = "http://192.168.0.164/desayunos/api_v1/usuario.php?mg=";
	public static final String URL = "http://compartedesayuno.com/api_v1/usuario.php?mg=";
	public static final String TAG_DEBUG = "Desayunos";
	public static final String PREFS_NAME = "desayunosPrefs";
	public static final int FRAGMENT_TAG_WRAPPER_ID = R.id.fragmentContentWrapper;
	
	// Metodos del servidor
	public static final String METHOD_LOGIN = "login";
	public static final String METHOD_UPDATE_BREAKFAST = "updatedesayuno";
	public static final String METHOD_USER = "user";
	public static final String METHOD_GO_BREAKFAST = "goBreakfast";
	public static final String METHOD_GET_GROUP_USERS = "groupUsers";
	public static final String METHOD_REGISTER = "register";
	public static final String METHOD_GET_ALL_GROUPS = "groups";
	public static final String METHOD_UPDATE_GROUP = "updateGrupo";
	public static final String METHOD_COMPARTE = "compartir";
	public static final String METHOD_RESUMEN = "resumen";
	public static final String METHOD_CREATRE_GROUP = "createBreakfastGroup";
	public static final String METHOD_SEARCH_BREAKFAST_GROUP = "searchBreakfastGroup";
	public static final String METHOD_PUSH_MY_GROUP = "pushMyBreakfastGroup";
	public static final String METHOD_CHANGE_PARTNER_ME_TOCA = "changeMeToca";
	
	// Publicidad
	public static final String ADMOB_ID = "ca-app-pub-6684414012298438/9461069901";
	
	// Analytics
	public static final String ANALYTICS_ID = "UA-59645124-1";

	// Parse
	public static final String PARSE_APPLICATION_ID = "8kvkTwLAF4c1aON0YzA1EGlaKxayLKQJqDI3vNK9";
	public static final String PARSE_CLIENT_KEY = "oIt3QT9NsFCUV7w4SYG8yvN18O2mz7dLcK1LfFtf";
}
