package com.mgonzalez.desayunos.application;

import java.util.List;

import org.json.JSONObject;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.mgonzalez.desayunos.activities.MainActivity;
import com.mgonzalez.desayunos.activities.ParentActivity;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.utils.Constants;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

public class MGApplication extends Application {

	/**
	 * Enum used to identify the tracker that needs to be used for tracking.
	 * 
	 * A single tracker is usually enough for most purposes. 
	 * In case you do need multiple trackers, storing them all in 
	 * Application object helps ensure that they are created only 
	 * once per application instance.
	 * 
	 */
	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
	}

	//private HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
	private Tracker tracker;

	private static final String US_LOGIN_TOKEN = "usLoginToken";
	private static final String USUARIO_AUTENTICADO = "usuarioAutenticado";

	private static MGApplication instance;
	private ParentActivity currentActivity;
	private String lt;
	private UsuarioAutenticadoDto usuarioAutenticado;

	public MGApplication() {
		instance = this;
	}

	public static MGApplication getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		new MGApplication();
		
		tracker = GoogleAnalytics.getInstance(this).newTracker(Constants.ANALYTICS_ID);
		
		Parse.initialize(this, Constants.PARSE_APPLICATION_ID, Constants.PARSE_CLIENT_KEY);
	}

	public ParentActivity getCurrentActivity() {
		return (ParentActivity) currentActivity;
	}

	public void setCurrentActivity(ParentActivity currentActivity) {
		this.currentActivity = currentActivity;
	}

	public String getLt() {
		return lt;
	}

	public void setLt(String lt) {
		this.lt = lt;
		guardaPreferencias();
	}

	public UsuarioAutenticadoDto getUsuarioAutenticado() {
		return usuarioAutenticado;
	}

	public void setUsuarioAutenticado(JSONObject usuarioAutenticado) {
		this.usuarioAutenticado = UsuarioAutenticadoDto.usuarioAutenticado(usuarioAutenticado);
		guardaPreferencias();
		
		List<String> channels = ParseInstallation.getCurrentInstallation().getList("channels");
		if(channels != null) {
			for(String channel : channels) {
				if(!channel.equals("c"+this.usuarioAutenticado.getGrupoId())) {
					ParsePush.unsubscribeInBackground(channel);
				}
			}
		}
		
		ParsePush.subscribeInBackground("c"+this.usuarioAutenticado.getGrupoId(), new SaveCallback() {
			  @Override
			  public void done(ParseException e) {
				  if (e == null) {
					  Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
				  } else {
					  Log.e("com.parse.push", "failed to subscribe for push", e);
				  }
			  }
		});

	}

	public void setUsuarioAutenticado(UsuarioAutenticadoDto usuarioAutenticado) {
		this.usuarioAutenticado = usuarioAutenticado;
		guardaPreferencias();
	}

	public boolean isUsuarioAutenticado() {
		return usuarioAutenticado != null && usuarioAutenticado.getLt() != null && !"".equals(usuarioAutenticado.getLt()) && lt != null && !"".equals(lt);
	}

	public SharedPreferences getSharedPreferences() {
		if (getCurrentActivity() != null) {
			return getCurrentActivity().getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
		} else {
			return getSharedPreferences(Constants.PREFS_NAME, MODE_PRIVATE);
		}
	}

	public void guardaPreferencias() {
		SharedPreferences prefs = getSharedPreferences();
		Editor editor = prefs.edit();
		if (usuarioAutenticado != null) {
			Gson gson = new Gson();
			String json = gson.toJson(usuarioAutenticado);
			editor.putString(USUARIO_AUTENTICADO, json);
		}
		if (lt != null) {
			editor.putString(US_LOGIN_TOKEN, lt);
		}
		editor.commit();
	}

	public void cargaPreferencias() {
		SharedPreferences prefs = getSharedPreferences();

		Gson gson = new Gson();
		String json = prefs.getString(USUARIO_AUTENTICADO, "");
		UsuarioAutenticadoDto aux = gson.fromJson(json, UsuarioAutenticadoDto.class);
		if (aux != null) {
			this.usuarioAutenticado = aux;
		}

		if (prefs.contains(US_LOGIN_TOKEN)) {
			this.lt = prefs.getString(US_LOGIN_TOKEN, "");
		}

	}

	public void logout() {
		UsuarioAutenticadoDto usuario = new UsuarioAutenticadoDto();
		setUsuarioAutenticado(usuario);
		setLt("");
		goLogin();
	}

	public void goLogin() {
		// TODO: Abrir la pantalla de login
		Intent intent = new Intent(getCurrentActivity(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getCurrentActivity().startActivity(intent);
	}

	// **************************
	// * 		Analytics		*
	// **************************
	public Tracker getTracker() {
		return tracker;
	}

}
