package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.TableLayout.LayoutParams;

import com.mgonzalez.desayunos.dto.UsuarioDto;

public class RowUser extends RadioButton{

	public RowUser(Context context, AttributeSet attrs) {
		super(context, attrs);
		setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(!isInEditMode()) {
			// TODO: Custom row
		}
	}
	
	public void setRow(UsuarioDto usuario) {
		setText(usuario.getNombre() + " " + usuario.getApellidos() + " (" + usuario.getUsername() + ")" + "\n" + usuario.getEmail());
	}

}
