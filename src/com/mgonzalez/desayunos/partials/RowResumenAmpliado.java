package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.dto.RowResumenDto;
import com.mgonzalez.desayunos.utils.Constants;

public class RowResumenAmpliado extends LinearLayout {

	private AQuery aq;
	
	public RowResumenAmpliado(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
	}
	
	public void setRow(RowResumenDto rowData) {
		if(rowData == null) {
			Log.e(Constants.TAG_DEBUG, "Datos nulos");
			return;
		}
		
		setRow(rowData.getNombreUsuario(), rowData.getTostada(), rowData.getBebida(), rowData.getNombreCompa(), rowData.getBebidaCompa());
	}
	private void setRow(String nombreUsuario, String tostada, String bebida, String nombreCompa, String bebidaCompa) {
		if(nombreCompa == null || "".equals(nombreCompa)) {
			aq.id(R.id.textNombre).text(nombreUsuario);
		} else {
			aq.id(R.id.textNombre).text(R.string.desayuno_compartido, nombreUsuario, nombreCompa);
		}
		
		aq.id(R.id.textDesayunoComida).text(R.string.amplia_tostada, tostada);
		
		if(bebidaCompa == null || "".equals(bebidaCompa)) {
			aq.id(R.id.textDesayunoBebidaCompa).gone();
			aq.id(R.id.textDesayunoBebida).text(R.string.amplia_bebida, bebida);
		} else {
			aq.id(R.id.textDesayunoBebidaCompa).visible().text(R.string.amplia_bebida_compa, bebidaCompa, nombreCompa);
			aq.id(R.id.textDesayunoBebida).text(R.string.amplia_bebida_compa, bebida, nombreUsuario);
		}
		
	}

}
