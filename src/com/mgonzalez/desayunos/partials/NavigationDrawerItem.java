package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;

public class NavigationDrawerItem extends LinearLayout {

	private AQuery aq;
	private NavigationDrawerItemListener listener;
	public interface NavigationDrawerItemListener {
		public void onItemClicked();
	}
	
	public NavigationDrawerItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		if(!isInEditMode()) {
			
			aq.id(R.id.btnItemMenu).clicked(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					listener.onItemClicked();
				}
			});
		}
	}

	public void setText(String title) {
		aq.id(R.id.btnItemMenu).text(title);
	}
	
	public void setText(int title) {
		String text = getResources().getString(title);
		setText(text);
	}
	
	public void setNavigationDrawerItemListener(NavigationDrawerItemListener listener) {
		this.listener = listener;
	}
	
	public void selected() {
		aq.id(R.id.btnItemMenu).textColor(getResources().getColor(R.color.green));
	}
}