package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;

public class MenuDrawer extends LinearLayout {
	
	private AQuery aq;

	public MenuDrawer(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		
		if(!isInEditMode()) {
			//TODO: Fuentes
		}
	}
	
	public LinearLayout getWrapperButtons() {
		LinearLayout wrapper = (LinearLayout)aq.id(R.id.wrapperMenuButtons).getView();
		if(wrapper != null) {
			return wrapper;
		} else {
			return this;
		}
	}

}