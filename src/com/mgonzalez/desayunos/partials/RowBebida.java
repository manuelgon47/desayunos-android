package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.utils.Constants;

public class RowBebida extends RelativeLayout{

	private AQuery aq;
	
	public RowBebida(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
		setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(!isInEditMode()) {
			// TODO: Custom row
		}
	}
	
	public void setRow(String bebida) {
		if(bebida == null) {
			Log.e(Constants.TAG_DEBUG, "Error: En RowGroup no se settea el nombre de la fila porque el objeto");
			return;
		}
		aq.id(R.id.titleBebida).text(bebida);
	}

}
