package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.activities.ParentActivity;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.mg.activity.MGParentActivity.MGDialogListener;

public class NavigationBar extends RelativeLayout {

	private AQuery aq;
	
	public NavigationBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(!isInEditMode()) {
			aq.id(R.id.btnLeft).clicked(this, "openMenu");
			aq.id(R.id.btnRight).clicked(this, "logout");
		}
	}
	
	public void setNavigationBarTitle(String title) {
		aq.id(R.id.navBarTitle).text(title);
	}
	
	public void setNavigationBarTitle(int resTitle) {
		aq.id(R.id.navBarTitle).text(resTitle);
	}
	
	public void setNavigationBarButtonLeft(int resDrawable) {
		aq.id(R.id.btnLeft).image(resDrawable);
	}
	
	public void setNavigationBarButtonRight(int resDrawable) {
		aq.id(R.id.btnRight).image(resDrawable);
	}
	
	public void setNavigationBarButtonLeftListener(OnClickListener listener) {
		aq.id(R.id.btnLeft).clicked(listener);
	}
	
	public void setNavigationBarButtonRightListener(OnClickListener listener) {
		aq.id(R.id.btnRight).clicked(listener);
	}
	
	public void openMenu() {
		ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.openMenuLateral();
		}
	}
	
	public void logout() {
		final ParentActivity activity = MGApplication.getInstance().getCurrentActivity();
		if(activity != null) {
			activity.showDialog(R.string.close_session, new MGDialogListener() {
				
				@Override
				public void cancelClick(DialogInterface dialog) {
					dialog.dismiss();
				}
				
				@Override
				public void acceptClick(DialogInterface dialog) {
					dialog.dismiss();
					activity.logout();
				}
			});
		}
	}

}
