package com.mgonzalez.desayunos.partials;

import org.json.JSONObject;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.utils.Constants;

public class RowPedidoIndividual extends LinearLayout{

	private AQuery aq;
	
	public RowPedidoIndividual(Context context, AttributeSet attrs) {
		super(context, attrs);
		aq = new AQuery(this);
		setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(!isInEditMode()) {
			// TODO: Custom row
		}
	}
	
	public void setRow(JSONObject json) {
		if(json == null) {
			Log.e(Constants.TAG_DEBUG, "Error: En RowPedidoIndividual no se settea el nombre de la fila porque el objeto");
			return;
		}
		
		aq.id(R.id.name).text(json.optString("nombre", ""));
		aq.id(R.id.tostada).text(getResources().getString(R.string.mi_desayuno_comida, json.optString("desayunoComida", "")));
		aq.id(R.id.cafe).text(getResources().getString(R.string.mi_desayuno_bebida, json.optString("desayunoBebida", "")));
		String comparte = json.optString("comparteCon", "");
		if(comparte != null && !"".equals(comparte)) {
			aq.id(R.id.comparteCon).text(getResources().getString(R.string.comparte_con, comparte));
		} else {
			aq.id(R.id.comparteCon).gone();
		}
		
	}

}
