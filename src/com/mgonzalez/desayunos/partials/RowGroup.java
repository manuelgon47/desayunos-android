package com.mgonzalez.desayunos.partials;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.TableLayout.LayoutParams;

import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.GrupoDto;
import com.mgonzalez.desayunos.dto.UsuarioAutenticadoDto;
import com.mgonzalez.desayunos.utils.Constants;

public class RowGroup extends RadioButton{
	
	private GrupoDto grupoDto;

	public RowGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
		setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(!isInEditMode()) {
			// TODO: Custom row
		}
	}
	
	public GrupoDto getGrupoDto() {
		return this.grupoDto;
	}
	
	public void setRow(GrupoDto grupo) {
		if(grupo == null) {
			Log.e(Constants.TAG_DEBUG, "Error: En RowGroup no se settea el nombre de la fila porque el objeto");
			return;
		}
		this.grupoDto = grupo;
		setText(grupo.getName());
		
		UsuarioAutenticadoDto usuarioAutenticado = MGApplication.getInstance().getUsuarioAutenticado();
		if(usuarioAutenticado != null) {
//			setChecked(usuarioAutenticado.getGrupoId() == grupo.getGrupoId());
//			if(usuarioAutenticado.getGrupoId() == grupo.getGrupoId()) {
//				this.performClick();
//			}
		}
	}

}
