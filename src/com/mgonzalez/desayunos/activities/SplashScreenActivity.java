package com.mgonzalez.desayunos.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.utils.Constants;

public class SplashScreenActivity extends ParentActivity {

	private int duration = Constants.DEBUG_MODE ? 100:2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		MGApplication.getInstance().cargaPreferencias();
		
		new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            	Class<? extends Activity> classToOpen = MainActivity.class;
            	
            	Bundle extras = new Bundle();
            	
            	if(MGApplication.getInstance().isUsuarioAutenticado()) {
            		classToOpen = IndexActivity.class;
                    extras.putBoolean(IndexActivity.PARAM_RELOAD, true);
            	}
            	
                Intent mainIntent = new Intent(SplashScreenActivity.this, classToOpen);
                mainIntent.putExtras(extras);
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, duration);
	}
	
}
