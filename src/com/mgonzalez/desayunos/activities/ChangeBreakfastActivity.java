package com.mgonzalez.desayunos.activities;

import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.fragment.ChangeBreakfastFragment;
import com.mgonzalez.desayunos.fragment.ParentFragment;

public class ChangeBreakfastActivity extends ParentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.template);
		
		openChangeBreakfastFragment();
	}
	
	private void openChangeBreakfastFragment() {
		ParentFragment fragment = new ChangeBreakfastFragment();
		openFragment(fragment, false);
	}
}
