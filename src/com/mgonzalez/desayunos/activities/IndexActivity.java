package com.mgonzalez.desayunos.activities;

import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.fragment.IndexFragment;
import com.mgonzalez.desayunos.fragment.ParentFragment;

public class IndexActivity extends ParentActivity {

	public static final String PARAM_RELOAD = "reloadData";
	
	public static boolean reload = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.template);
		
		openIndexFragment();
	}

	private void openIndexFragment() {
		ParentFragment fragment = new IndexFragment();
		openFragment(fragment, false);
	}
	
}
