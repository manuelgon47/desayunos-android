package com.mgonzalez.desayunos.activities;

import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.fragment.ChangeBreakfastGroupFragment;
import com.mgonzalez.desayunos.fragment.ParentFragment;

public class ChangeBreakfastGroupActivity extends ParentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.template);
		
		openChangeBreakfastGroupFragment();
	}
	
	private void openChangeBreakfastGroupFragment() {
		ParentFragment fragment = new ChangeBreakfastGroupFragment();
		openFragment(fragment, false);
	}
}
