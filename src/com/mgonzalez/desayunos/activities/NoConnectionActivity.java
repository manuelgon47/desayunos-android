package com.mgonzalez.desayunos.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.androidquery.AQuery;
import com.mgonzalez.desayunos.R;

public class NoConnectionActivity extends ParentActivity {

	private AQuery aq;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.no_connection);
		aq = new AQuery(this);
		aq.id(R.id.btnReintentar).clicked(this, "onBackPressed");
		aq.id(R.id.btnSalir).clicked(this, "exitApp");
	}
	

	
	@Override
	public void onBackPressed() {
		if(isNetworkAvailable()) {
			super.onBackPressed();
		}
	}
	
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	public void exitApp() {
		finish();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
}
