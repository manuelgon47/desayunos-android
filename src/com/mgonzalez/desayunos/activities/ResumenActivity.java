package com.mgonzalez.desayunos.activities;

import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.fragment.ParentFragment;
import com.mgonzalez.desayunos.fragment.ResumenFragment;

public class ResumenActivity extends ParentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.template);
		
		openResumenFragment();
	}
	
	private void openResumenFragment() {
		ParentFragment fragment = new ResumenFragment();
		openFragment(fragment, false);
	}

}
