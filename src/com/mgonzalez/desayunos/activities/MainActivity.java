package com.mgonzalez.desayunos.activities;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.utils.Constants;

public class MainActivity extends ParentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		if(Constants.DEBUG_MODE) {
			aq.id(R.id.editUsername).text("mgon");
			aq.id(R.id.editPasswd).text("123456");
		}
		
		aq.id(R.id.btnLogin).clicked(this, "doLogin");
		aq.id(R.id.btnRegister).clicked(this, "openRegister");

	}
	
	public void openRegister() {
		openIntent(new Intent(this, RegisterActivity.class));
	}
	
	public void doLogin() {
		String us = aq.id(R.id.editUsername).getText().toString();
		String pw = aq.id(R.id.editPasswd).getText().toString();
		if(us == null || "".equals(us)) {
			showToast(R.string.username_vacio);
			return;
		}
		if(pw == null || "".equals(pw)) {
			showToast(R.string.passwd_vacio);
			return;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("us", us);
		params.put("pw", MD5(pw));
		
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_LOGIN, params, new ParentDtoListener() {
			
			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject usuario = result.optJSONObject("usuario");
					if(usuario != null) {
						String lt = usuario.optString("lt", "");
						MGApplication.getInstance().setLt(lt);
						MGApplication.getInstance().setUsuarioAutenticado(usuario);
						openIntent(new Intent(MainActivity.this, IndexActivity.class));
					} else {
						showToast(R.string.error_inesperado);
					}
				}
				
			}
			
			@Override
			public void onFailureExecuteRemoteCall(String message, int httpCode) {
				hideLoading();
			}
		});
	}

}
