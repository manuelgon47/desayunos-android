package com.mgonzalez.desayunos.activities;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.utils.Constants;

public class RegisterActivity extends ParentActivity {

	public static final String PARAM_RELOAD = "reloadData";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		aq.id(R.id.btnRegister).clicked(this, "validate");
	}
	
	public void validate() {
		String username = aq.id(R.id.editUsername).getText().toString();
		String passwd = aq.id(R.id.editPasswd).getText().toString();
		String name = aq.id(R.id.editName).getText().toString();
//		String surname = aq.id(R.id.editSurname).getText().toString();
		String email = aq.id(R.id.editEmail).getText().toString();
		String desaComida = aq.id(R.id.editDesaComida).getText().toString();
		String desaBebida = aq.id(R.id.editDesaBebida).getText().toString();
		
		if("".equals(username)) {
			showToast("El campo nombre de usuario el obligatorio");
			return;
		} else if("".equals(passwd)) {
			showToast("El campo contraseña el obligatorio");
			return;
		} else if("".equals(email)) {
			showToast("El campo email el obligatorio");
			return;
		} else if("".equals(name)) {
			showToast("El campo nombre el obligatorio");
			return;
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		params.put("passwd", MD5(passwd));
		params.put("nombre", name);
		params.put("apellidos", "");
		params.put("email", email);
		params.put("desayunoComida", desaComida);
		params.put("desayunoBebida", desaBebida);
		register(params);
	}
	
	private void register(Map<String, Object> params) {
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_REGISTER, params, new ParentDtoListener() {

			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if("error".equals(estado) || "".equals(estado)) {
					if("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}
				
				if("correcto".equals(estado)) {
					JSONObject usuario = result.optJSONObject("usuario");
					if(usuario != null) {
						String lt = usuario.optString("lt", "");
						MGApplication.getInstance().setLt(lt);
						MGApplication.getInstance().setUsuarioAutenticado(usuario);
						openIntent(new Intent(RegisterActivity.this, IndexActivity.class));
					} else {
						showToast(R.string.error_inesperado);
					}
				}			
			}

			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				hideLoading();
			}
			
		});
	}
	
	@Override
	public void onBackPressed() {
		openIntent(MainActivity.class);
	}
	
}
