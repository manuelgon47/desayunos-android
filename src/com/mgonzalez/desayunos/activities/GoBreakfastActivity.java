package com.mgonzalez.desayunos.activities;

import android.os.Bundle;

import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.fragment.GoBreakfastFragment;
import com.mgonzalez.desayunos.fragment.ParentFragment;

public class GoBreakfastActivity extends ParentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.template);
		
		openGoBreakfastFragment();
	}
	
	private void openGoBreakfastFragment() {
		ParentFragment fragment = new GoBreakfastFragment();
		openFragment(fragment, false);
	}
}
