package com.mgonzalez.desayunos.activities;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.mgonzalez.desayunos.R;
import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.dto.ParentDto;
import com.mgonzalez.desayunos.dto.ParentDto.ParentDtoListener;
import com.mgonzalez.desayunos.fragment.ParentFragment;
import com.mgonzalez.desayunos.partials.MenuDrawer;
import com.mgonzalez.desayunos.partials.NavigationBar;
import com.mgonzalez.desayunos.partials.NavigationDrawerItem;
import com.mgonzalez.desayunos.partials.NavigationDrawerItem.NavigationDrawerItemListener;
import com.mgonzalez.desayunos.utils.Constants;
import com.mgonzalez.mg.activity.MGParentActivity;

public class ParentActivity extends MGParentActivity {
	
	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aq = new AQuery(this);
		MGApplication.getInstance().setCurrentActivity(this);

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		// Crear adView.
	    adView = new AdView(this);
	    adView.setAdUnitId(Constants.ADMOB_ID);
	    adView.setAdSize(AdSize.BANNER);
	    
	    LinearLayout layout = (LinearLayout)findViewById(R.id.banner);
	    if(layout != null) {
	    	layout.removeAllViews();
	    	// Añadirle adView.
	    	layout.addView(adView);

	        // Iniciar una solicitud genérica.
	        AdRequest adRequest = new AdRequest.Builder().build();
	        
//            String android_id = android.provider.Settings.Secure.getString(this.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
//            String deviceId = MD5(android_id).toUpperCase();
//            Log.d(Constants.TAG_DEBUG, "DeviceId: " + deviceId);
//        	adRequest = new AdRequest.Builder()
//            .addTestDevice(deviceId) // Mi teléfono de prueba Galaxy Nexus
//            .build();

	        // Cargar adView con la solicitud de anuncio.
	        adView.loadAd(adRequest);
	    }
	}
	
	@Override
	public void onPause() {
		if(adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	@Override
	public void onDestroy() {
		if(adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initMenuLateral();
	}
	
	public void openIntent(Class<?> classToOpen) {
		int flags = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
		openIntent(classToOpen, flags);
	}
	
	public void openIntent(Class<?> classToOpen, int flags) {
		Intent intent = new Intent(this, classToOpen);
		intent.addFlags(flags);
		openIntent(intent);
	}

	public void openIntent(Intent intent) {
		openIntent(intent, 0, 0, -1);
	}

	public void openIntent(Intent intent, int enterAnim, int exitAnim, int requestCode) {
		if (requestCode >= 0) {
			startActivityForResult(intent, requestCode);
		} else {
			startActivity(intent);
		}
		overridePendingTransition(enterAnim, exitAnim);
	}

	public void showToast(String mensaje) {
		try {
			Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
		} catch (Exception e) {
		}
	}

	public void showToast(int mensaje) {
		String text = getString(mensaje);
		if (text != null && !text.equals("")) {
			showToast(text);
		}
	}

	public void exitApp() {
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}

	public void closeKeyboard(EditText editText) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	public String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}

	public void logout() {
		MGApplication.getInstance().logout();
	}

	public void goBreakfast(Map<String, Object> params) {
		if (params == null) {
			Log.e(Constants.TAG_DEBUG, "Error en goBrakfast de ParentActivity, el mapa es nulo");
			return;
		}
		showLoading();
		ParentDto.getInstance().remoteCallPost(Constants.URL, Constants.METHOD_GO_BREAKFAST, params, new ParentDtoListener() {

			@Override
			public void onPostExecuteRemoteCall(JSONObject result) {
				hideLoading();
				String estado = result.optString("estado", "");
				String msg = result.optString("msg", "");
				if ("error".equals(estado) || "".equals(estado)) {
					if ("".equals(msg)) {
						showToast(R.string.error_inesperado);
					} else {
						showToast(msg);
					}
				}

				if ("correcto".equals(estado)) {
					showToast(msg);
				}
			}

			@Override
			public void onFailureExecuteRemoteCall(String msg, int httpCode) {
				hideLoading();
			}

		});
	}

	// ******************************
	// *		MENU LATERAL		*
	// ******************************
	private void initMenuLateral() {
		MenuDrawer drawer = (MenuDrawer) findViewById(R.id.left_drawer);
		final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		if (drawer != null && drawerLayout != null) {

			LinearLayout wrapperButtons = drawer.getWrapperButtons();
			wrapperButtons.removeAllViews();
			
			SortedMap<Integer, Class<?>> opciones = new TreeMap<Integer, Class<?>>();
			opciones.put(R.string.menu_index, IndexActivity.class);
			opciones.put(R.string.menu_change_breakfast, ChangeBreakfastActivity.class);
			opciones.put(R.string.menu_go_breakfast, GoBreakfastActivity.class);
			opciones.put(R.string.menu_resumen, ResumenActivity.class);
			opciones.put(R.string.menu_change_group, ChangeBreakfastGroupActivity.class);

			final Iterator<Entry<Integer, Class<?>>> it = opciones.entrySet().iterator();
			Entry<Integer, Class<?>> entry = null;
			while (it.hasNext()) {
				entry = it.next();
				final int resTitle = entry.getKey();
				final Class<?> classToOpen = entry.getValue();
				NavigationDrawerItem item = (NavigationDrawerItem) View.inflate(this, R.layout._navigation_drawer_item, null);
				
				item.setText(resTitle);
				item.setNavigationDrawerItemListener(new NavigationDrawerItemListener() {

					@Override
					public void onItemClicked() {
						ParentActivity currentActivity = MGApplication.getInstance().getCurrentActivity();
						if(currentActivity != null && currentActivity.getClass().equals(classToOpen)) {
							closeMenuLateral();
							return;
						}
						if (classToOpen != null) {
							closeMenuLateral();
							openIntent(classToOpen);
						}
					}
				});
				wrapperButtons.addView(item);
			}

		}
	}
	
	private DrawerLayout getMenuLateral() {
		return (DrawerLayout)aq.id(R.id.drawer_layout).getView();
	}
	
	public void openMenuLateral() {
		DrawerLayout menu = getMenuLateral();
		if(menu != null) {
			menu.openDrawer(Gravity.LEFT);
		}
	}
	
	public void closeMenuLateral() {
		DrawerLayout menu = getMenuLateral();
		if(menu != null) {
			menu.closeDrawers();
		}
	}
	
	// **********************
	// *		TabBar		*
	// **********************
	public NavigationBar getNavBar() {
		return (NavigationBar)aq.id(R.id.navBar).getView();
	}
	
	// **********************
	// *		Otros		*
	// **********************
	private boolean doubleBackToExitPressedOnce = false;
	@Override
	public void onBackPressed() {
		ParentFragment fragment = getCurrentFragment(Constants.FRAGMENT_TAG_WRAPPER_ID);
		if(fragment == null) {
			super.onBackPressed();
			return;
		}
		FragmentManager fm = fragment.getFragmentManager();
		if(fm == null) {
			super.onBackPressed();
			return;
		}
	    if (doubleBackToExitPressedOnce || fm.getBackStackEntryCount() > 0) {
	        super.onBackPressed();
	        return;
	    }

	    this.doubleBackToExitPressedOnce = true;
	    Toast.makeText(this, R.string.exit_double_back, Toast.LENGTH_SHORT).show();

	    new Handler().postDelayed(new Runnable() {

	        @Override
	        public void run() {
	            doubleBackToExitPressedOnce=false;                       
	        }
	    }, 2000);
	}

}
