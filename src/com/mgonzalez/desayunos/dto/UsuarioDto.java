package com.mgonzalez.desayunos.dto;

import org.json.JSONObject;

import android.support.v4.os.ParcelableCompat;

public class UsuarioDto extends ParcelableCompat {
	private Integer usuarioId;
	private String nombre;
	private String apellidos;
	private String email;
	private String desayunoComida;
	private String desayunoBebida;
	private String username;
	private Integer grupoId;
	private String grupo;
	private Integer actualPartnerId;
	private String actualPartnerName;
	private boolean voyADesayunar;

	public UsuarioDto() {
		this.usuarioId = 0;
		this.nombre = "";
		this.apellidos = "";
		this.email = "";
		this.desayunoComida = "";
		this.desayunoBebida = "";
		this.username = "";
		this.grupoId = 0;
		this.grupo = "";
		this.actualPartnerId = 0;
		this.actualPartnerName = "";
		this.voyADesayunar = false;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDesayunoComida() {
		return desayunoComida;
	}

	public void setDesayunoComida(String desayunoComida) {
		this.desayunoComida = desayunoComida;
	}

	public String getDesayunoBebida() {
		return desayunoBebida;
	}

	public void setDesayunoBebida(String desayunoBebida) {
		this.desayunoBebida = desayunoBebida;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(Integer grupoId) {
		this.grupoId = grupoId;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Integer getActualPartnerId() {
		return actualPartnerId;
	}

	public void setActualPartnerId(Integer actualPartnerId) {
		this.actualPartnerId = actualPartnerId;
	}

	public String getActualPartnerName() {
		return actualPartnerName;
	}

	public void setActualPartnerName(String actualPartnerName) {
		this.actualPartnerName = actualPartnerName;
	}

	public boolean isVoyADesayunar() {
		return voyADesayunar;
	}

	public void setVoyADesayunar(boolean voyADesayunar) {
		this.voyADesayunar = voyADesayunar;
	}

	public static UsuarioDto loadUser(JSONObject json) {
		UsuarioDto us = new UsuarioDto();
		us.setUsuarioId(json.optInt("usuarioId", 0));
		us.setNombre(json.optString("nombre", ""));
		us.setApellidos(json.optString("apellidos", ""));
		us.setEmail(json.optString("email", ""));
		us.setDesayunoComida(json.optString("desayunoComida", ""));
		us.setDesayunoBebida(json.optString("desayunoBebida", ""));
		us.setUsername(json.optString("username", ""));
		us.setGrupoId(json.optInt("grupoId", 0));
		us.setGrupo(json.optString("grupo", ""));
		us.setActualPartnerId(json.optInt("actualPartnerId", -1));
		us.setActualPartnerName(json.optString("actualPartnerName", ""));
		us.setVoyADesayunar(json.optInt("voy", 0) == 1);

		return us;
	}

}
