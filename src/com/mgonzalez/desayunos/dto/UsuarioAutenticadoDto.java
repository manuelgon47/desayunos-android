package com.mgonzalez.desayunos.dto;

import org.json.JSONObject;

public class UsuarioAutenticadoDto extends UsuarioDto {
	private String lt;
	private boolean meTocaPagar;

	public UsuarioAutenticadoDto() {
		this.lt = "";
		this.meTocaPagar = false;
	}

	public String getLt() {
		return lt;
	}

	public void setLt(String lt) {
		this.lt = lt;
	}

	public boolean isMeTocaPagar() {
		return meTocaPagar;
	}

	public void setMeTocaPagar(boolean meTocaPagar) {
		this.meTocaPagar = meTocaPagar;
	}

	public static UsuarioAutenticadoDto usuarioAutenticado(JSONObject json) {
		UsuarioAutenticadoDto us = new UsuarioAutenticadoDto();
		us.setNombre(json.optString("nombre", ""));
		us.setApellidos(json.optString("apellidos", ""));
		us.setEmail(json.optString("email", ""));
		us.setDesayunoComida(json.optString("desayunoComida", ""));
		us.setDesayunoBebida(json.optString("desayunoBebida", ""));
		us.setUsername(json.optString("username", ""));
		us.setGrupoId(json.optInt("grupoId", 0));
		us.setGrupo(json.optString("grupo", ""));
		us.setActualPartnerId(json.optInt("actualPartnerId", 0));
		us.setActualPartnerName(json.optString("actualPartnerName", ""));
		us.setLt(json.optString("lt", ""));
		us.setVoyADesayunar(json.optInt("voy", 0) == 1);
		us.setMeTocaPagar(json.optInt("meToca", 0) == 1);
		us.setMeTocaPagar(json.optBoolean("meToca", false));

		return us;
	}

}
