package com.mgonzalez.desayunos.dto;

import org.json.JSONObject;

public class RowResumenDto {
	
	private String nombreUsuario;
	private String tostada;
	private String bebida;
	private String nombreCompa;
	private String bebidaCompa;
	
	private RowResumenDto() {
		this.nombreUsuario = "";
		this.tostada = "";
		this.bebida = "";
		this.nombreCompa = "";
		this.bebidaCompa = "";
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getTostada() {
		return tostada;
	}

	public void setTostada(String tostada) {
		this.tostada = tostada;
	}

	public String getBebida() {
		return bebida;
	}

	public void setBebida(String bebida) {
		this.bebida = bebida;
	}

	public String getNombreCompa() {
		return nombreCompa;
	}

	public void setNombreCompa(String nombreCompa) {
		this.nombreCompa = nombreCompa;
	}

	public String getBebidaCompa() {
		return bebidaCompa;
	}

	public void setBebidaCompa(String bebidaCompa) {
		this.bebidaCompa = bebidaCompa;
	}
	
	public static RowResumenDto parseRowResumenDto(JSONObject json) {
		RowResumenDto ret = new RowResumenDto();
		if(json != null) {
			ret.setNombreUsuario(json.optString("nombre", "") + " " + json.optString("apellidos", ""));
			ret.setTostada(json.optString("desayunoComida", ""));
			ret.setBebida(json.optString("desayunoBebida", ""));
			JSONObject compa = json.optJSONObject("compa");
			if(compa != null) {
				ret.setNombreCompa(compa.optString("nombre", "") + " " + compa.optString("apellidos", ""));
				ret.setBebidaCompa(compa.optString("desayunoBebida", ""));
			}
		}
		 return ret;
	}

}
