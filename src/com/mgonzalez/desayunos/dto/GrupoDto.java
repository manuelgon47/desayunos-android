package com.mgonzalez.desayunos.dto;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.mgonzalez.desayunos.utils.Constants;

public class GrupoDto {
	
	private int grupoId;
	private String name;
	private String emailAdmin;
	
	public GrupoDto() {
		this.grupoId = 0;
		this.name = "";
		this.emailAdmin = "";
	}

	public int getGrupoId() {
		return grupoId;
	}

	public void setGrupoId(int grupoId) {
		this.grupoId = grupoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmailAdmin() {
		return emailAdmin;
	}

	public void setEmailAdmin(String emailAdmin) {
		this.emailAdmin = emailAdmin;
	}

	//***********************
	//*			UTIL		*
	//***********************
	public static List<GrupoDto> loadGrupos(JSONObject json) {
		List<GrupoDto> ret = new ArrayList<GrupoDto>();
		if(json == null) {
			Log.e(Constants.TAG_DEBUG, "Error, el json de grupos viene vacío");
			return ret;
		}
		
		JSONArray grupos = json.optJSONArray("grupos");
		for(int i = 0; i < grupos.length(); i++) {
			GrupoDto aux = new GrupoDto();
			JSONObject grupo = grupos.optJSONObject(i);
			aux.setGrupoId(grupo.optInt("grupoid", 0));
			aux.setName(grupo.optString("nombre", ""));
			aux.setEmailAdmin(grupo.optString("adminGrupo", ""));
			ret.add(aux);
		}
		
		return ret;
	}
	
}
