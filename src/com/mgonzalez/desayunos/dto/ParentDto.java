package com.mgonzalez.desayunos.dto;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.mgonzalez.desayunos.application.MGApplication;
import com.mgonzalez.desayunos.utils.Constants;


public class ParentDto {

	private static ParentDto instance;

	public abstract interface ParentDtoListener {
		public void onPostExecuteRemoteCall(JSONObject result);
		public void onFailureExecuteRemoteCall(String msg, int httpCode);
	}

	public ParentDto() {
		instance = this;
	}

	public static ParentDto getInstance() {
		if (instance == null) {
			new ParentDto();
		}
		return instance;
	}

	public void remoteCallGet(final String url, final String method, final Map<String, Object> hashMap, final ParentDtoListener listener) {

		AsyncTask<String, Integer, JSONObject> service = new AsyncTask<String, Integer, JSONObject>() {

			protected JSONObject doInBackground(String... arg0) {
				String urlCall = url + method;
				
				String lt = MGApplication.getInstance().getLt();
				if(lt != null && !"".equals(lt)) {
					urlCall += "&lt=" + lt;
				}
				
				if(hashMap != null) {
					for(String key : hashMap.keySet()) {
						urlCall += "&" + key + "=" + hashMap.get(key);
					}
				}
				
				HttpClient httpClient = new DefaultHttpClient();

				HttpGet httpget = new HttpGet(urlCall);
				JSONObject json = new JSONObject();
				try {
					HttpResponse response = httpClient.execute(httpget);
					if(response.getStatusLine().getStatusCode()== 401) {
						String msg = "Su sesión ha caducado";
						int code = 0;
						code = response.getStatusLine().getStatusCode();
						if(listener != null) {
							listener.onFailureExecuteRemoteCall(msg, code);
						}
						if(code == 401) {
							MGApplication.getInstance().logout();
						}
						return json;
					}
					if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
						if(listener != null) {
							listener.onFailureExecuteRemoteCall(null, 0);
						}
						return json;
					}
					// Leemos el JSON
					json = new JSONObject(EntityUtils.toString(response.getEntity()));

				} catch (HttpResponseException e) {
					String msg = "Su sesión ha caducado";
					int code = 0;
					code = e.getStatusCode();
					if(listener != null) {
						listener.onFailureExecuteRemoteCall(msg, code);
					}
					Log.e(Constants.TAG_DEBUG, e.getMessage());
					if(code == 401) {
						MGApplication.getInstance().goLogin();
					}
				} catch(Exception e) {
					String msg = "";
					int code = 0;
					if(listener != null) {
						listener.onFailureExecuteRemoteCall(msg, code);
					}
					Log.e(Constants.TAG_DEBUG, e.getMessage());
				}

				return json;
			}

			protected void onPostExecute(JSONObject result) {
				if(listener != null) {
					listener.onPostExecuteRemoteCall(result);
				}
			}

		};
		service.execute();

	}

	public void remoteCallPost(final String url, final String method, final Map<String, Object> params, final ParentDtoListener listener) {

		AsyncTask<String, Integer, String> service = new AsyncTask<String, Integer, String>() {

			@SuppressWarnings({ "unchecked" })
			protected String doInBackground(String... arg0) {
				HttpClient httpclient = new DefaultHttpClient();
				ResponseHandler<String> handler = new BasicResponseHandler();
				//JSONObject jsonRet = new JSONObject();
				String result = "";
				try {
					String urlCall = url + method;
					
					String lt = MGApplication.getInstance().getLt();
					if(lt != null && !"".equals(lt)) {
						urlCall += "&lt=" + lt;
					}
					
					MultipartEntity entity = new MultipartEntity();
					
					// Parámetros:
					Set<String> keys = params.keySet();
			    	for (String key : keys) {
			    		Object valor = params.get(key);
			    		
			    		if(valor instanceof List){
							List<String> lista = (List<String>)valor;
			    			
			    			for (String item : lista) {
			    				entity.addPart(key, new StringBody(item, Charset.forName(HTTP.UTF_8)));
							}
				    		
			    		}else if(valor instanceof File){
			    			entity.addPart(key, new FileBody((File)valor));
			    			
			    		}else if(valor != null){
		    				entity.addPart(key, new StringBody(valor.toString(), Charset.forName(HTTP.UTF_8)));
						    
			    		}else{
		    				entity.addPart(key, new StringBody("", Charset.forName(HTTP.UTF_8)));
			    		}
			    	}
			    	
			    	HttpPost request = new HttpPost(urlCall);
				    request.setEntity(entity);
			        result = httpclient.execute(request, handler);

					// Leemos el JSON
					//jsonRet = new JSONObject(EntityUtils.toString(response.getEntity()));

				} catch (HttpResponseException e) {
					String msg = "Su sesión ha caducado";
					int code = 0;
					code = e.getStatusCode();
					
					if(listener != null) {
						listener.onFailureExecuteRemoteCall(msg, code);
					}
					Log.e(Constants.TAG_DEBUG, e.getMessage());
					if(code == 401) {
						MGApplication.getInstance().goLogin();
					}
				} catch(Exception e) {
					String msg = "";
					int code = 0;
					if(listener != null) {
						listener.onFailureExecuteRemoteCall(msg, code);
					}
					Log.e(Constants.TAG_DEBUG, e.getMessage());
				}

				return result;
			}

			protected void onPostExecute(String result) {
				JSONObject ret = new JSONObject();
				try {
					ret = new JSONObject(result);
				} catch (Exception e) {
					Log.e(Constants.TAG_DEBUG, e.getMessage());
					if(listener != null) {
						listener.onFailureExecuteRemoteCall(null, 0);
					}
				}
				if(listener != null) {
					listener.onPostExecuteRemoteCall(ret);
				}
			}

		};
		service.execute();

	}
/*	
	private class CargaRemotaServer extends AsyncTask<Map<String, Object>, Void, String> {

		@Override
		@SuppressWarnings("unchecked")
        protected String doInBackground(Map<String, Object>... params) {
        	//initialize
        	String result = "";
     
			try {
				Map<String, Object> datos = params[0];
				String url = Constants.URL;
				String urlMethod = (String) datos.get(METHOD_URL);
				boolean esPost = tipoPeticion.equals(ZSRemoteDto.FORM_METHOD_POST);
				HttpClient httpclient = new DefaultHttpClient();
			    ResponseHandler<String> handler = new BasicResponseHandler();
			    MultipartEntity entity = new MultipartEntity();
	    
		    	//El resto de datos son parámetros
			    Set<String> keys = datos.keySet();
		    	for (String key : keys) {
		    		Object valor = datos.get(key);
		    		
		    		if(key.equals(ZSRemoteDto.FORM_URL) || key.equals(ZSRemoteDto.FORM_METHOD)){
		    			continue;
		    		}
		    		
		    		if(valor instanceof List){
						List<String> lista = (List<String>)valor;
		    			
		    			for (String item : lista) {
		    				entity.addPart(key, new StringBody(item, Charset.forName(HTTP.UTF_8)));
						}
			    		
		    		}else if(valor instanceof File && esPost){
		    			entity.addPart(key, new FileBody((File)valor));
		    			
		    		}else if(valor != null){
	    				entity.addPart(key, new StringBody(valor.toString(), Charset.forName(HTTP.UTF_8)));
					    
		    		}else{
	    				entity.addPart(key, new StringBody("", Charset.forName(HTTP.UTF_8)));
		    		}
				}
			    
			    if(esPost){
				    HttpPost request = new HttpPost(url);
				    request.setEntity(entity);
			        result = httpclient.execute(request, handler);  
			    }else{
			    	HttpGet request = new HttpGet(url);
			        result = httpclient.execute(request, handler);  
			    }
			    httpclient.getConnectionManager().shutdown();
				
        	}catch(Exception e){
        		Log.e(Constants.TAG_DEBUG, "Error al traer o convertir el contenido remoto "+e.toString());
        		
        		//Llamamos a los métodos correspondiente para el caso de listados y activities
        		if(listener != null && listener instanceof ZSRemoteDtoResponseListener){
        			((ZSRemoteDtoResponseListener)listener).remoteDtoServerError(item, action, "Error al traer o convertir el contenido remoto "+e.toString());
        		}
        	}
            
    		return result;
        }      

        @Override
        protected void onPostExecute(String result) {
        	procesaRespuesta(result);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
	}   	
*/


}
